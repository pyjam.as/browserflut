FROM python:3.8-slim-buster
RUN pip install quart hypercorn
WORKDIR /
COPY ./server.py .
COPY ./index.html .
CMD ["hypercorn", "--bind", "0.0.0.0:80", "server:app"]
