import asyncio
from asyncio import Queue, start_server
from quart import Quart, websocket, send_from_directory
import socket

app = Quart(__name__)
clients = set()


@app.route("/")
async def index():
    return await send_from_directory("/", "index.html")


@app.websocket("/display")
async def display_endpoint():
    c = Queue()
    clients.add(c)
    while True:
        data = await c.get()
        await websocket.send(data)


@app.websocket("/")
async def drawing_endpoint():
    while True:
        data = await websocket.receive()
        await broadcast(data)


async def broadcast(data):
    for c in clients:
        await c.put(data)


async def handle_tcp(reader, writer):
    while True:
        data = await reader.readuntil(b"\n")
        message = data.decode()
        await broadcast(message)


@app.before_first_request
async def run_tcp_server():
    print("Starting TCP Server")
    await start_server(handle_tcp, "0.0.0.0", 1337)
